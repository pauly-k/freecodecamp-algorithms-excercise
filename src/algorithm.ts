export function find_symentric_difference(...arrays: number[][]) {
  let items = arrays.reduce((arr1, arr2) => [
    ...arr1.filter(entry => arr2.indexOf(entry) < 0),
    ...arr2.filter(entry => arr1.indexOf(entry) < 0)
  ])

  const set = new Set<number>()
  for (let item of items) {
    set.add(item)
  }
  return [...set].sort()
}

export function sortCompareStringGenerator(ascending = true) {
  return function(a: any, b: any) {
    if (ascending) {
      return a < b ? -1 : 1
    } else {
      return a < b ? 1 : -1
    }
  }
}

export function updateInventory(curInv: any[], newInv: any[]) {
  // for each updated item
  //   if already in curINv, then update count
  //   else insert
  for (let newItem of newInv) {
    let currentItem = curInv.find(([count, title]) => title === newItem[1])
    if (currentItem) {
      currentItem[0] += newItem[0]
    } else {
      curInv.push(newItem)
    }
  }

  return [...curInv].sort((a, b) => (a[1] < b[1] ? -1 : 1))
}

export function palindrome(word: string) {
  let mid = Math.floor(word.length / 2)
  for (let i = 0; i <= mid; i++) {
    let a = word[i]
    let b = word.slice(-(i + 1))[0]
    if (a !== b) {
      return false
    }
  }
  return true
}

export function uniqueCharacterCount(str: string) {
  const set = new Set()
  str.split('').forEach(char => {
    if (!set.has(char)) {
      set.add(char)
    }
  })
  return set.size
}

export function factorial(n: number) {
  let sum = 1
  for (let i = 1; i <= n; i++) {
    sum *= i
  }
  return sum
}

export function incrementInteger(arr: number[]): number[] {
  let carry = 1
  for (let i = arr.length - 1; i >= 0; i--) {
    let val = arr[i] + carry
    if (val > 9) {
      arr[i] = 0
      carry = val - 9
    } else {
      arr[i] = val
      carry = 0
    }
  }

  if (carry > 0) {
    arr.unshift(carry)
  }

  return arr
}

export function fibonacci(n: number) {
  let [a, b] = [0, 1]

  if (n === 1) {
    return a
  }
  if (n === 2) {
    return b
  }

  for (let i = 3; i <= n; i++) {
    ;[a, b] = [b, a + b]
  }
  return b
}

export function fibonacciRecursive_1(n: number): number {
  if (n === 1) return 0
  if (n === 2) return 1

  return fibonacciRecursive_1(n - 1) + fibonacciRecursive_1(n - 2)
}

export function fibonacciRecursive_Memoize(n: number, memo?: number[]): number {
  if (n === 1) {
    return 0
  }
  if (n === 2) {
    return 1
  }
  memo = memo || []
  if (!memo[n]) {
    memo[n] =
      fibonacciRecursive_Memoize(n - 1, memo) +
      fibonacciRecursive_Memoize(n - 2, memo)
  }

  return memo[n]
}

export function reverseString(str: string): string {
  let arr = str.split('')
  let mid =
    str.length % 2 === 0
      ? Math.floor(arr.length / 2) - 1
      : Math.floor(arr.length / 2)
  let l = arr.length
  for (let i = 0; i <= mid; i++) {
    let k = l - (i + 1)
    ;[arr[i], arr[k]] = [arr[k], arr[i]]
  }
  return arr.join('')
}

export function absoluteDifference(arr1: number[], arr2: number[]): number {
  let diff = 0
  for (const i in arr1) {
    diff += Math.abs(arr1[i] - arr2[i])
  }
  return diff
}

export function absoluteDifference_r(
  a: number[],
  b: number[],
  size?: number
): number {
  size = (size === undefined ? a.length : size) as number
  if (size <= 0) return 0
  return (
    Math.abs(a[size - 1] - b[size - 1]) + absoluteDifference_r(a, b, size - 1)
  )
}

interface HoursMinutes {
  hours: number
  minutes: number
}
export function timeStrToHoursMinutes(hhmm: string): HoursMinutes {
  //@ts-ignore
  let result = hhmm.split(':').reduce((a,b) => { return { hours: +a, minutes: +b }})
  return result as HoursMinutes
}

export function timeDiff(startHHMM: string, stopHHMM: string) {
  const start = timeStrToHoursMinutes(startHHMM)
  const stop = timeStrToHoursMinutes(stopHHMM)

  const diffMinutes = (stop.hours * 60 + stop.minutes) - (start.hours * 60 + start.minutes)

  const hours = Math.floor(diffMinutes / 60)
  const minutes = diffMinutes % 60

  return `${hours.toString().padStart(2, '0')}:${minutes
    .toString()
    .padStart(2, '0')}`
}
