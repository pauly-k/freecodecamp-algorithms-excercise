import {
  find_symentric_difference,
  sortCompareStringGenerator,
  palindrome,
  uniqueCharacterCount,
  factorial,
  updateInventory,
  incrementInteger,
  fibonacci,
  fibonacciRecursive_1,
  fibonacciRecursive_Memoize,
  reverseString,
  absoluteDifference,
  absoluteDifference_r,
  timeStrToHoursMinutes,
  timeDiff
} from './algorithm'

describe('freecodecamp algorithsm', () => {
  test('find_symentric_difference', () => {
    const tests = [
      {
        sets: [
          [1, 2, 3],
          [5, 2, 1, 4]
        ],
        result: [3, 4, 5]
      },
      {
        sets: [
          [1, 2, 3, 3],
          [5, 2, 1, 4]
        ],
        result: [3, 4, 5]
      },
      {
        sets: [
          [1, 2, 3],
          [5, 2, 1, 4, 5]
        ],
        result: [3, 4, 5]
      },
      {
        sets: [
          [1, 2, 5],
          [2, 3, 5],
          [3, 4, 5]
        ],
        result: [1, 4, 5]
      },
      {
        sets: [
          [1, 1, 2, 5],
          [2, 2, 3, 5],
          [3, 4, 5, 5]
        ],
        result: [1, 4, 5]
      },
      {
        sets: [
          [3, 3, 3, 2, 5],
          [2, 1, 5, 7],
          [3, 4, 6, 6],
          [1, 2, 3]
        ],
        result: [2, 3, 4, 6, 7]
      }
    ]

    for (let test of tests) {
      let result = find_symentric_difference(...test.sets)
      expect(result).toEqual(test.result)
    }
  })

  test('sort compare string fn generator', () => {
    const tests = [
      {
        list: ['zodiac', 'queue', 'sand'],
        ascending: true,
        result: ['queue', 'sand', 'zodiac']
      },
      {
        list: ['zodiac', 'queue', 'sand'],
        ascending: false,
        result: ['zodiac', 'sand', 'queue']
      },
      {
        list: ['Farnam', 'Street', 'Blog'],
        ascending: true,
        result: ['Blog', 'Farnam', 'Street']
      },
      {
        list: ['Farnam', 'Street', 'Blog'],
        ascending: false,
        result: ['Street', 'Farnam', 'Blog']
      }
    ]

    let sortFn
    for (let test of tests) {
      sortFn = sortCompareStringGenerator(test.ascending)
      let result = test.list.sort(sortFn)
      expect(result).toEqual(test.result)
    }
  })

  test('update inventory', () => {
    const tests = [
      {
        curInv: [
          [21, 'Bowling Ball'],
          [2, 'Dirty Sock'],
          [1, 'Hair Pin'],
          [5, 'Microphone']
        ],
        newInv: [
          [2, 'Hair Pin'],
          [3, 'Half-Eaten Apple'],
          [67, 'Bowling Ball'],
          [7, 'Toothpaste']
        ],
        result: [
          [88, 'Bowling Ball'],
          [2, 'Dirty Sock'],
          [3, 'Hair Pin'],
          [3, 'Half-Eaten Apple'],
          [5, 'Microphone'],
          [7, 'Toothpaste']
        ]
      },
      {
        curInv: [
          [21, 'Bowling Ball'],
          [2, 'Dirty Sock'],
          [1, 'Hair Pin'],
          [5, 'Microphone']
        ],
        newInv: [],
        result: [
          [21, 'Bowling Ball'],
          [2, 'Dirty Sock'],
          [1, 'Hair Pin'],
          [5, 'Microphone']
        ]
      },
      {
        curInv: [],
        newInv: [
          [2, 'Hair Pin'],
          [3, 'Half-Eaten Apple'],
          [67, 'Bowling Ball'],
          [7, 'Toothpaste']
        ],
        result: [
          [67, 'Bowling Ball'],
          [2, 'Hair Pin'],
          [3, 'Half-Eaten Apple'],
          [7, 'Toothpaste']
        ]
      },

      {
        curInv: [
          [0, 'Bowling Ball'],
          [0, 'Dirty Sock'],
          [0, 'Hair Pin'],
          [0, 'Microphone']
        ],
        newInv: [
          [1, 'Hair Pin'],
          [1, 'Half-Eaten Apple'],
          [1, 'Bowling Ball'],
          [1, 'Toothpaste']
        ],
        result: [
          [1, 'Bowling Ball'],
          [0, 'Dirty Sock'],
          [1, 'Hair Pin'],
          [1, 'Half-Eaten Apple'],
          [0, 'Microphone'],
          [1, 'Toothpaste']
        ]
      }
    ]

    for (let test of tests) {
      let result = updateInventory(test.curInv, test.newInv)
      expect(result).toEqual(test.result)
    }
  })

  test('palindrome', () => {
    const tests = [
      { word: 'civic', palindrome: true },
      { word: 'cleo', palindrome: false },
      { word: 'level', palindrome: true },
      { word: 'carirar', palindrome: false },
      { word: 'a', palindrome: true },
      { word: 'wow', palindrome: true }
    ]

    tests.forEach(test => {
      const result = palindrome(test.word)
      expect(result).toEqual(test.palindrome)
    })
  })

  test('uniqueCharCount', () => {
    const tests = [
      { str: 'aab', result: 2 },
      { str: 'aaa', result: 1 },
      { str: 'aabb', result: 2 },
      { str: 'abc', result: 3 },
      { str: 'aaabb', result: 2 }
    ]

    tests.forEach(test => {
      const result = uniqueCharacterCount(test.str)
      expect(result).toEqual(test.result)
    })
  })

  test('factorail', () => {
    const tests = [
      { n: 3, result: 6 },
      { n: 4, result: 24 },
      { n: 5, result: 120 }
    ]

    tests.forEach(test => {
      const result = factorial(test.n)
      expect(result).toEqual(test.result)
    })
  })

  test('increment integer', () => {
    const tests = [
      { input: [9], output: [1, 0] },
      { input: [1, 2, 3], output: [1, 2, 4] },
      { input: [1, 9, 9], output: [2, 0, 0] },
      { input: [9, 9], output: [1, 0, 0] }
    ]

    tests.forEach(test => {
      const result = incrementInteger(test.input)
      expect(result).toEqual(test.output)
    })
  })
})

describe('back to back swe algorithsm', () => {
  test('fibonacci', () => {
    const tests = [
      { n: 1, finonacci: 0 },
      { n: 2, finonacci: 1 },
      { n: 3, finonacci: 1 },
      { n: 4, finonacci: 2 },
      { n: 5, finonacci: 3 },
      { n: 6, finonacci: 5 },
      { n: 7, finonacci: 8 }
    ]

    tests.forEach(test => {
      const result = fibonacci(test.n)
      expect(result).toEqual(test.finonacci)
    })
  })

  test('fibonacci recursive', () => {
    const tests = [
      { n: 1, finonacci: 0 },
      { n: 2, finonacci: 1 },
      { n: 3, finonacci: 1 },
      { n: 4, finonacci: 2 },
      { n: 5, finonacci: 3 },
      { n: 6, finonacci: 5 },
      { n: 7, finonacci: 8 }
    ]

    tests.forEach(test => {
      const result = fibonacciRecursive_1(test.n)
      expect(result).toEqual(test.finonacci)
    })
  })

  test('fibonacci recursive memoize', () => {
    const tests = [
      { n: 1, finonacci: 0 },
      { n: 2, finonacci: 1 },
      { n: 3, finonacci: 1 },
      { n: 4, finonacci: 2 },
      { n: 5, finonacci: 3 },
      { n: 6, finonacci: 5 },
      { n: 7, finonacci: 8 }
    ]

    tests.forEach(test => {
      const result = fibonacciRecursive_Memoize(test.n)
      expect(result).toEqual(test.finonacci)
    })
  })

  test('reverseString', () => {
    let tests = [['pauly', 'yluap']]

    for (let test of tests) {
      let [input, exptecedOutout] = test
      const r = reverseString(input)
      expect(r).toEqual(exptecedOutout)
    }
  })

  test('aobsolute difference interative', () => {
    let tests = [
      {
        a: [0, 4, 9, -3],
        b: [4, 9, -2, 1],
        diff: 24
      },
      {
        a: [10, 4, 9, -3, 2],
        b: [4, 9, -2, 1, -9],
        diff: 37
      }
    ]

    tests.forEach(test => {
      const r = absoluteDifference(test.a, test.b)
      expect(r).toEqual(test.diff)
    })
  })
  test('aobsolute difference recursive ', () => {
    let tests = [
      {
        a: [0, 4, 9, -3],
        b: [4, 9, -2, 1],
        diff: 24
      },
      {
        a: [10, 4, 9, -3, 2],
        b: [4, 9, -2, 1, -9],
        diff: 37
      }
    ]

    tests.forEach(({a, b, diff}) => {
      const r = absoluteDifference_r(a, b, a.length)
      expect(r).toEqual(diff)
    })
  })

  test('hours minutes from time string', () => {
    const tests = [
      {
        timeStr: '09:30',
        result: {
          hours: 9,
          minutes: 30
        }
      },
      {
        timeStr: '00:02',
        result: {
          hours: 0,
          minutes: 2
        }
      },
    ]

    tests.forEach(test =>{
      const result = timeStrToHoursMinutes(test.timeStr)
      expect(result).toEqual(test.result)
    })
  })

  test('time diff', () => {
    const tests = [
      {
        start: "09:20",
        stop: "10:40",
        diff: "01:20"
      }, 
      {
        start: "19:20",
        stop: "23:10",
        diff: "03:50"
      }, 
    ]

    tests.forEach(({start, stop, diff}) => {
      const result = timeDiff(start, stop)
      expect(result).toEqual(diff)
    })
  })

})
